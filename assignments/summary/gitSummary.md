# **What is GiT ?**
Git is the way we manage code and there is no software development without git.
Git is version-control software that makes collaboration with teammates super simple.
Git lets you easily keep track of every revision you and your team make during the development of your software.
Basically, if you’re not using git, you’re coding with one hand tied behind your back.

**Repository**:
A repository is the collection of files and folders (code files) that you’re using git to track.

**GitLab**:

**Commit**:
Think of this as saving your work to a remote repository.

**Push**:
Pushing is essentially syncing your commits to GitLab.

**Branch**:
You can think of your git repo as a tree and the branches are separate instances of the code that is different from the main codebase.

**Merge**:
Merging is just integrating two branches together when they are free of bugs.

**Clone**:
Cloning a repo means making an exact copy of it on your local machine.

**Fork**:
Forking is done to get an entirely new repo of that code under your own name.

![](https://i.redd.it/nm1w0gnf2zh11.png)

## **Git Internals**
Git has three main states that your files can reside in: modified, staged, and committed :

- Workspace : All the changes you make via Editor(s) (gedit, notepad, vim, nano) is done in this tree of repository.
- Staging : All the staged files go into this tree of your repository.
- Local Repository : All the committed files go to this tree of your repository.
- Remote Repository : This is the copy of your Local Repository but is stored in some server on the Internet. All the changes you
- commit into the Local Repository are not directly reflected into this tree. You need to push your changes to the Remote Repository.

### **Git Workflow and terminal commands**

**For Linux:** open the terminal and type to install GiT
    
    $ sudo apt-get install git

**For Windows:** Download the git installer and run it  
    

Git work flow
![](https://docs.gitlab.com/ee/topics/img/gitlab_flow_four_stages.png)
clone the repo

    $ git clone <link-to-repository> 

Create a new branch

    $ git checkout master
    $ git checkout -b <your-branch-name>

modify files in your working tree.
selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.

    $ git add .          # To add untracked files ( . adds all files) 

You do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Local Git Repository.

    $ git commit -sv  # Description about the commit

You do a push, which takes the files as they are in the Local Git Repository and stores that snapshot permanently to your Remote Git Repository.

    $ git push origin <branch-name>  # push changes into repository

![](https://user-content.gitlab-static.net/e14f5dfd7d52cd11c0385d7fc0267c3b75b1eccd/68747470733a2f2f7777772e7475746f7269616c73706f696e742e636f6d2f6769742f696d616765732f6c6966655f6379636c652e706e67)

# **Merge Request Workflow**
Almost all projects follow this style of workflow. If any projects don't follow this style then the mentors will let you know the workflow in advance.

Creating a branch to organize all your work
The first step is to create a branch that you work in.
Remember, one Issue one branch. Any suggestions by mentor or changes to code related to the Issue happens in the respective branch.

- **Step 1 : Go to the Project that you are currently working on and Click on Issues** 

- **Step 2 : Select Issue that you are working on**

- **Step 3 : Click on the dropdown arrow**

- **Step 4 : Select Create Branch and Click on Create branch**



## **Working on code for the particular Issue**


- **Step 5: Clone the Project**

**Run these commands on your Laptop/Desktop**
    
    $ git clone <url-of-the-repo>
    $ git checkout <branch-name>

_Note:Branch name starts with the issue number_


- **Step 6: Committing the code and Pushing it upstream**

Make changes into the library.
   
    $ git add .
    $ git commit -sv
    $ git push

**Step 7: Checklist for Submitting the Merge Request**

Go over all the following points:


- [ ]  My code follows the coding style of this project (IoTIoT coding Style).
 
- [ ]  My code compiles and links without errors on the Shunya OS Docker container.

- [ ]  My code is documented in the IoTIoT coding style (Doxygen) format.
 
- [ ]  My change requires a change to the User/Developer documentation.

- [ ]  I have updated/submitted the User/Developer documentation accordingly.


1. 
**Step 8.1: Go to Your Project, Click on Create Merge Request**

Go over all the following points:


- [ ] My Merge Request provides a general summary of your changes in the Title.

- [ ]  My Merge Request description contains the "type of change" (i.e New Feature or Bug-Fix).

- [ ]  My Merge Request Passes all the conditions in the Checklist.
